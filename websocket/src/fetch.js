const WebSocket = require('ws');
const crypto = require('crypto');
const fs = require("fs");

// settings
var settings = JSON.parse(fs.readFileSync("settings.json")).default;
const access_key = settings.access_key;
const secret_key = settings.secret_key;

// server start
console.log("Fetch ws data from: " + settings.name);
var ws = new WebSocket(settings.url);
var opened = false;

ws.onopen = function(msg) {
    console.log('----------------opened----------------');
}
// message handler
ws.onmessage = function(msg) {
    try {
        const ob = JSON.parse(msg.data).orderbook;
        const action = ob.action;
        const order = ob.order;
        const sign = action == "remove" ? "-" : "+";
        console.log(sign + ' ' + JSON.stringify(order));
    } catch (e){
        //pass on first auth message
    }

    if (!opened) {
        const challenge = JSON.parse(msg.data).challenge;
        const payload = access_key + challenge;
        const signature = crypto.createHmac('SHA256', secret_key).update(payload).digest('hex');
        console.log("----------------signiture: " + signature + "----------------");
        ws.send(JSON.stringify({
            auth: {
                access_key: access_key, 
                answer: signature
            }
        }));
        opened = true;
        console.log("----------------ws sent msg----------------");
    }
};

ws.onclose = function() {
    console.log('closed');
}
