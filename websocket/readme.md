# acx price data fetch:
node 1.js | tee data_logs/log_file_`date +'%Y-%m-%d-%H-%M-%S'`.json"
# btc market close price etc:
btc_eth_aud.json capture Last Price, Bid, Ask:
{ volume24h: 491760855173,
  bestBid: 191270000000,
  bestAsk: 191900000000,
  lastPrice: 190590000000,
  timestamp: 1515913149865,
  snapshotId: 1515913149865000,
  marketId: 2010,
  currency: 'AUD',
  instrument: 'ETH' }
# btc market order book
