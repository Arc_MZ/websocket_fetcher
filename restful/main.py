from btcmarkets import BTCMarkets
from config_template import api_key, private_key
import time, json, datetime

if __name__ == '__main__':
    client = BTCMarkets (api_key, private_key)
    #print client.trade_history('AUD', 'BTC', 10, 1)
    #print client.order_detail([1234, 213456])
    #print client.order_create('AUD', 'LTC', 100000000, 100000000, 'Bid', 'Limit', '1')
    #print client.get_market_tick('ETH','AUD')
    with open("data/{}_orderbook.json".format(time.time()), "w") as f:

        while True:
            start = time.time()
            print("sent     on: {}".format(start))
            obr = client.get_market_orderbook('ETH','AUD')
            # print(datetime.datetime.utcfromtimestamp(
            #     obr['timestamp']
            # ).strftime('%H:%M:%SZ'))
            
            f.write(json.dumps(obr) + ',\n')
            print("received on: {}".format(obr['timestamp']))
            cost = time.time() - start
            time.sleep(min(5, 5 - cost)) # when api reject e.g. 7s cost, we adjust to 5 